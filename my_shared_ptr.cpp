#include "pch.h"
#include <cstddef>      
#include <algorithm>
#include <cassert>

class shared_ptr_count
{
public:
	shared_ptr_count() : pn(NULL){} 
	shared_ptr_count(const shared_ptr_count& count) : pn(count.pn){}
	//copy constructor
	void swap(shared_ptr_count& lhs) 
	{
		std::swap(pn, lhs.pn);
	}
	//counter getter
	long use_count(void) const 
	{
		long count = 0;
		if (NULL != pn)
		{
			count = *pn;
		}
		return count;
	}
	// acquire/share the ownership of the reference counter pointer
	template<class U>
	void acquire(U* p) 
	{
		if (NULL != p)
		{
			if (NULL == pn)
			{
				try
				{
					pn = new long(1); //throw std::bad_alloc
				}
				catch (std::bad_alloc&)
				{
					delete p;
					throw; 
				}
			}
			else
			{
				++(*pn);
			}
		}
	}
	//release the px ownership
	template<class U>
	void release(U* p) 
	{
		if (NULL != pn)
		{
			--(*pn);
			if (0 == *pn)
			{
				delete p;
				delete pn;
			}
			pn = NULL;
		}
	}

public:
	long*   pn; //Reference counter
};



template<class T>
class shared_ptr
{
public:
	//default constructor
	shared_ptr(void) : px(NULL), pn() {}
	//constructor with pointer to manage
	explicit shared_ptr(T* p) : pn()
	{
		acquire(p);   // may throw std::bad_alloc
	}
	//copy constructor
	shared_ptr(const shared_ptr& ptr) : pn(ptr.pn)
	{
		assert((NULL == ptr.px) || (0 != ptr.pn.use_count()));
		acquire(ptr.px);   
	}
	
	//copy operator
	shared_ptr& operator=(shared_ptr ptr)
	{
		swap(ptr);
		return *this;
	}
	//destructor
	inline ~shared_ptr(void) 
	{
		release();
	}
	//copy constructor
	void swap(shared_ptr& lhs)
	{
		std::swap(px, lhs.px);
		pn.swap(lhs.pn);
	}
	// reference counter operations 
	inline operator bool() const 
	{
		return (0 < pn.use_count());
	}
	inline bool unique(void)  const 
	{
		return (1 == pn.use_count());
	}
	long use_count(void)  const 
	{
		return pn.use_count();
	}

	inline T& operator*()  const 
	{
		assert(NULL != px);
		return *px;
	}
	inline T* operator->() const 
	{
		assert(NULL != px);
		return px;
	}
	inline T* get(void)  const throw() // never throws
	{
		// no assert, can return NULL
		return px;
	}
private:
	//init counter
	inline void acquire(T* p)
	{
		pn.acquire(p);
		px = p; 
	}
	//destroy pointer
	inline void release(void) 
	{
		pn.release(px);
		px = NULL;
	}

private:
	T*                  px; //Native pointer
	shared_ptr_count    pn; //Reference counter
};


#include<iostream>

int main()
{
	shared_ptr<int> x_ptr(new int(42));
	shared_ptr<int> y_ptr(new int(13));
	
	y_ptr = x_ptr;

	std::cout << *x_ptr << "\t" << *y_ptr << std::endl;
	std::cout << x_ptr.use_count() << std::endl;
	y_ptr.~shared_ptr();
	std::cout << x_ptr.use_count();
	
	return 0;
}